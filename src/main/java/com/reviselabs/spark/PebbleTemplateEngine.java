/*
 * Copyright 2016 - Kevin Sheppard
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.reviselabs.spark;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.loader.ClasspathLoader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;
import spark.ModelAndView;
import spark.TemplateEngine;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 * A Template Engine that uses the Pebble template engine @see <a href="http://www.mitchellbosecke.com/pebble/home">mitchellbosecke.com/pebble/home</a>
 *
 * @author Kevin Sheppard
 */
public class PebbleTemplateEngine extends TemplateEngine {
	private PebbleEngine engine;

	/**
	 * Construct a new PebbleTemplateEngine with reasonable defaults.
	 */
	public PebbleTemplateEngine() {
		ClasspathLoader loader = new ClasspathLoader();
		loader.setPrefix("views");
		loader.setSuffix(".pbl");
		engine = new PebbleEngine
				.Builder()
				.loader(loader)
				.build();
	}

	/**
	 * Render a view without passing any data in. Just a helper method.
	 * @param view The name of the view file (without the extension).
	 * @return A String of the compiled template.
	 */
	public String render(String view) {
		return render(null, view);
	}

	/**
	 * The generic rendering method. Uses general boilerplate from @see <a href="http://www.mitchellbosecke.com/pebble/home">mitchellbosecke.com/pebble/home</a>
	 * @param data A Map of keys and values that you want to pass to the template.
	 * @param view The name of the view file (without the extension).
	 * @return A String of the compiled template.
	 */
	public String render(Map<String, Object> data, String view) {
		StringWriter writer = new StringWriter();
		try {
			PebbleTemplate compiledTemplate = engine.getTemplate(view);
			if(data != null) {
				compiledTemplate.evaluate(writer, data);
			} else {
				compiledTemplate.evaluate(writer);
			}
			return writer.toString();
		} catch (PebbleException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Render method specific to the Spark Framework.
	 * @param modelAndView The ModelAndView class passed to all Spark routes.
	 * @return A String of the compiled template.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public String render(ModelAndView modelAndView) {
		Object data = modelAndView.getModel();
		if(!(data instanceof Map))
			throw new IllegalArgumentException("View data must be instance of java.util.Map.");
		return render((Map<String, Object>) data, modelAndView.getViewName());
	}

	/**
	 * Sets the default file extension for view files. Default: .pebble
	 * @param defaultExtension
	 */
	public void setDefaultExtension(String defaultExtension) {
		engine.getLoader().setSuffix(defaultExtension);
	}

	/**
	 * Sets the folder where the rendering engine will look for view files
	 * relative to the resources folder. Default: views
	 * @param viewsFolder
	 */
	public void setViewsFolder(String viewsFolder) {
		engine.getLoader().setPrefix(viewsFolder);
	}
}
