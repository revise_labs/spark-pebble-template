package com.reviselabs.spark;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import spark.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kevin Sheppard
 */
public class PebbleTemplateEngineTest {
    PebbleTemplateEngine engine;
    String result;
    Map<String, Object> data;

    @Before
    public void setUp() {
        engine = new PebbleTemplateEngine();
        data = new HashMap<>();
    }

    @Test
    public void testBasicRendering() {
        result = engine.render("basic");
        Assert.assertTrue(result.contains("Hello"));
    }

    @Test
    public void testGenericRendererWithData() {
        data.put("name", "Kevin");
        result = engine.render(data, "greeting");
        Assert.assertTrue(result.contains("Kevin"));
    }

    @Test
    public void testViewModelRendering() {
        data.put("name", "Kevin");
        ModelAndView view = new ModelAndView(data, "greeting");
        result = engine.render(view);
        Assert.assertTrue(result.contains("Kevin"));
    }
}
