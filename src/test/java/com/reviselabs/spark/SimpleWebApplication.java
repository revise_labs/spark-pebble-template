package com.reviselabs.spark;

import spark.ModelAndView;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;

/**
 * Created by Kevin on 5/10/2016.
 */

public class SimpleWebApplication {
    public static void main(String[] args) {
        PebbleTemplateEngine pebble = new PebbleTemplateEngine();

        get("/", (req, res) -> {
            Map<String, Object> data = new HashMap<>();
            data.put("name", "Kevin");
               return new ModelAndView(data, "index");
        }, pebble);
    }
}
