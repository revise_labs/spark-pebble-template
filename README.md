spark-template-pebble
==============================================

How to use the Pebble template engine for Spark example:

```java
package spark.template.pebble;

import spark.ModelAndView;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;

/**
 * @author Kevin Sheppard
 */

public class SimpleWebApplication {
    public static void main(String[] args) {
        PebbleTemplateEngine pebble = new PebbleTemplateEngine();

        get("/", (req, res) -> {
            Map<String, Object> data = new HashMap<>();
            data.put("name", "Kevin");
            //index.pbl file in resources/views/index.pbl
            return new ModelAndView(data, "index");
        }, pebble);
    }
}
```

### Customization

#### The Default Views Folder

By default, the template engine looks for views in the `resources/views` folder, but you can change that by calling
the `setViewsFolder()` method and passing in the path of the views folder relative to the `resources` folder.

E.g.

```java
PebbleTemplateEngine pebble = new PebbleTemplateEngine();
pebble.setViewsFolder("templates/pebble");

```

This will cause the instance of the template engine to look for views in the `resources/templates/pebble` folder.

### The Default File Extension

The template engine looks for files with the `.pbl` extension by default. You can change this by calling the `setDefaultExtension()`
method and passing in the new extension for all your views.

E.g.

```java
PebbleTemplateEngine pebble = new PebbleTemplateEngine();
pebble.setDefaultExtension(".html");
```
This saves you from having to enter the file extension every time you need to create a ModelAndView object.